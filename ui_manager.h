/* vim: set ai noet ts=4 sw=4 tw=115: */
//
// Copyright (c) 2014 Nikolay Zapolnov (zapolnov@gmail.com).
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
#ifndef __9c3c650d3a1cf3da04b118a2552e06d1__
#define __9c3c650d3a1cf3da04b118a2552e06d1__

#include "ui_delegate_factory.h"
#include <functional>
#include <unordered_map>
#include <memory>
#include <yip-imports/tinyxml.h>
#include <yip-imports/cxx-util/make_ptr.h>
#include <yip-imports/glm/vec2.hpp>

namespace UI
{
	class Manager;
	class Element;
	class ElementDelegate;

	/** Strong pointer to the UI manager. */
	typedef std::shared_ptr<Manager> ManagerPtr;

	/** UI manager. */
	class Manager : public std::enable_shared_from_this<Manager>
	{
	public:
		/** Pointer to callback function to be called when setNeedsLayout() has been called on this manager. */
		std::function<void()> onNeedsLayout;

		/**
		 * Creates new instance of the UI manager.
		 * @param origSize Original size of the UI.
		 * @return Pointer to the instance of the UI manager.
		 */
		static ManagerPtr create(const glm::vec2 & origSize);

		/**
		 * Creates new instance of the UI manager from the given XML document.
		 * @param xmlElem XML element describing the manager.
		 * @param dfact Factory for UI element delegates (optional).
		 * @return Pointer to the instance of the UI manager.
		 */
		static ManagerPtr createFromXML(const TiXmlElement * xmlElem,
			const DelegateFactory & dfact = DelegateFactory());

		/** Destructor. */
		~Manager();

		/**
		 * Returns original size of the UI.
		 * @return Original size of the UI.
		 */
		const glm::vec2 & originalSize() const { return m_OriginalSize; }

		/**
		 * Returns current size of the viewport.
		 * @return Current size of the viewport.
		 */
		const glm::vec2 & viewportSize() const { return m_ViewportSize; }

		/**
		 * Returns current scale of the UI.
		 * @return Current scale of the UI.
		 */
		const glm::vec2 & scale() const { return m_Scale; }

		/**
		 * Sets size of the viewport.
		 * @param viewSize New size of the viewport.
		 */
		void setViewportSize(const glm::vec2 & viewSize);

		/**
		 * Creates new UI element.
		 * Element is created at the root level and its lifetime is controlled by the UI::Manager instance.
		 * If you add the created element as a child into another element, its lifetime will be managed by
		 * the parent element.
		 * @param id Unique ID for the element.
		 * @param delegate Pointer to the delegate for the element. Delegate is responsible for
		 * destroying itself after receiving the UI::ElementDelegate::onElementDestroyed callback.
		 */
		Element * createElement(const std::string & id = std::string(), ElementDelegate * delegate = nullptr);

		/**
		 * Loads UI element hierarchy from an XML document.
		 * @param xmlElem XML element describing the root UI element.
		 * @param dfact Factory for UI element delegates (optional).
		 * @param parent Parent UI element (optional).
		 * @return Pointer to the root UI element.
		 * @throw std::runtime_error if element creation fails.
		 */
		Element * createElementFromXML(const TiXmlElement * xmlElem,
			const DelegateFactory & dfact = DelegateFactory(), Element * parent = nullptr);

		/**
		 * Checks whether layout of elements is out of date.
		 * @return *true* if layout of UI elements should be recalculated, *false* otherwise.
		 */
		inline bool needsLayout() const { return m_NeedsLayout; }

		/** Sets an internal flag indicating that layouting of UI elements should be performed. */
		void setNeedsLayout();

		/**
		 * Checks whether UI manager is performing layouting right now.
		 * @return *true* if UI manager is performing layouting, *false* otherwise.
		 */
		inline bool layouting() const { return m_Layouting; }

		/**
		 * Performs layouting of UI elements controlled by this manager.
		 * This method resets an internal flag set by setNeedsLayout().
		 */
		void layoutItems();

		/**
		 * Retrieves UI element by it's unique ID.
		 * @param id Unique ID of the element.
		 * @return pointer to the UI element or *nullptr* if there is no element with the specified unique ID.
		 */
		Element * getElementById(const std::string & id) const;

	private:
		std::unordered_map<std::string, Element *> m_Elements;
		glm::vec2 m_OriginalSize;
		glm::vec2 m_ViewportSize;
		glm::vec2 m_Scale;
		Element * m_FirstRootElement;
		Element * m_LastRootElement;
		bool m_Layouting;
		bool m_NeedsLayout;

		Manager(const glm::vec2 & origSize);

		void addRootElement(Element * element);
		void removeRootElement(Element * element);

		Manager(const Manager &) = delete;
		Manager & operator=(const Manager &) = delete;

		FRIEND_MAKE_PTR();
		friend class Element;
	};
}

#endif
