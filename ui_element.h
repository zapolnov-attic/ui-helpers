/* vim: set ai noet ts=4 sw=4 tw=115: */
//
// Copyright (c) 2014 Nikolay Zapolnov (zapolnov@gmail.com).
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
#ifndef __75bc67be150a87626b3dafb88de581da__
#define __75bc67be150a87626b3dafb88de581da__

#include "ui_manager.h"
#include "ui_size_constraint.h"
#include "ui_align.h"
#include "ui_aspect_mode.h"
#include <yip-imports/cxx-util/make_ptr.h>
#include <yip-imports/glm/vec2.hpp>
#include <memory>

namespace UI
{
	class Layout;
	class ElementDelegate;

	/** Base class for UI elements. */
	class Element
	{
	public:
		/**
		 * Pointer to the delegate for the element.
		 *
		 * Note that delegate is responsible for destroying itself when the
		 * UI::ElementDelegate::onElementDestroyed callback is called.
		 *
		 * If you change delegate from one to another, *you* are responsible
		 * for memory management of the old delegate.
		 */
		ElementDelegate * delegate = nullptr;

		/**
		 * Destructor.
		 * Destroys all child elements of this element.
		 */
		~Element();

		/**
		 * Returns unique ID of the element.
		 * @return Unique ID of the element.
		 */
		inline const std::string & id() const { return m_ID; }

		/**
		 * Returns pointer to the UI manager controlling this element.
		 * @return Pointer to the UI manager.
		 */
		const ManagerPtr & manager() const { return m_Manager; }

		/**
		 * Returns current position of this element relative to the parent element.
		 * @return Coordinates of the top left corner of this element.
		 */
		inline const glm::vec2 & position() const { return m_Position; }

		/**
		 * Returns size of this element.
		 * @return Size of this element.
		 */
		inline const glm::vec2 & size() const { return m_Size; }

		/**
		 * Returns desired position of this element relative to the parent element.
		 * This position serves as a hint to the layouting system.
		 * @return Desired coordinates of the top left corner of this element.
		 */
		inline const glm::vec2 & desiredPosition() const { return m_DesiredPosition; }

		/**
		 * Returns desired size of this element.
		 * This size serves as a hint to the layouting system.
		 * @return Desired size of this element.
		 */
		inline const glm::vec2 & desiredSize() const { return m_DesiredSize; }

		/**
		 * Checks whether desired size was set for this element.
		 * @return *true* if desired size was set for this element, *false* otherwise.
		 */
		inline bool hasDesiredSize() const { return m_HasDesiredSize; }

		/**
		 * Returns pointer to the layout which manages children of this element.
		 * @return Pointer to the layout which manages children of this element.
		 */
		const Layout * layout() const;

		/**
		 * Returns pointer to the parent UI element.
		 * @return Pointer to the parent UI element or *nullptr* if there is no parent element.
		 */
		inline Element * parent() const { return m_Parent; }

		/**
		 * Returns pointer to the first child UI element.
		 * @return Pointer to the first child UI element or *nullptr* if there is are no children elements.
		 */
		inline Element * firstChild() const { return m_FirstChild; }

		/**
		 * Returns pointer to the last child UI element.
		 * @return Pointer to the last child UI element or *nullptr* if there is are no children elements.
		 */
		inline Element * lastChild() const { return m_LastChild; }

		/**
		 * Returns pointer to the next sibling UI element.
		 * @return Pointer to the next sibling UI element or *nullptr* if this is the last element in the list.
		 */
		inline Element * nextSibling() const { return m_Next; }

		/**
		 * Returns pointer to the previous sibling UI element.
		 * @return Pointer to the previous sibling UI element or *nullptr* if this is the first element in the list.
		 */
		inline Element * prevSibling() const { return m_Prev; }

		/**
		 * Returns relative weight of the item.
		 * @return Relative weight of the item.
		 */
		inline float weight() const { return m_Weight; }

		/**
		 * Returns alignment of the item.
		 * @return Alignment of the item.
		 */
		inline Align align() const { return m_Align; }

		/**
		 * Returns horizontal alignment of the item.
		 * @return Horizontal alignment of the item.
		 */
		inline Align horzAlign() const { return static_cast<Align>(m_Align & ALIGN_HORIZONTAL_BITMASK); }

		/**
		 * Returns vertical alignment of the item.
		 * @return Vertical alignment of the item.
		 */
		inline Align vertAlign() const { return static_cast<Align>(m_Align & ALIGN_VERTICAL_BITMASK); }

		/**
		 * Returns aspect preservation mode of the item.
		 * @return Aspect preservation mode of the item.
		 */
		inline AspectMode aspectMode() const { return m_AspectMode; }

		/**
		 * Returns aspect preservation mode for the top left corner of the item.
		 * @return Aspect preservation mode for the top left corner of the item.
		 */
		inline AspectMode positionAspectMode() const { return m_PositionAspectMode; }

		/**
		 * Sets a layout that should manage children of this element.
		 * @param layout Pointer to the layout. If this is *nullptr* then the default layout will be used.
		 */
		void setLayout(const Layout * layout);

		/**
		 * Sets desired position for this element relative to the parent element.
		 * This position serves as a hint to the layouting system.
		 * @param pos Desired coordinates of the top left corner for this element.
		 */
		void setDesiredPosition(const glm::vec2 & pos);

		/**
		 * Sets desired size for this element.
		 * This size serves as a hint to the layouting system.
		 * @param sz Desired size for this element.
		 */
		void setDesiredSize(const glm::vec2 & sz);

		/** Resets the desired size for this element. */
		void resetDesiredSize();

		/**
		 * Sets relative weight of the item.
		 * @param w Relative weight of the item.
		 */
		void setWeight(float w);

		/**
		 * Sets alignment of the item.
		 * @param align Alignment of the item.
		 */
		void setAlign(Align alignment);

		/**
		 * Sets aspect ratio preservation mode of the item.
		 * @param mode Aspect ratio preservation mode of the item.
		 */
		void setAspectMode(AspectMode mode);

		/**
		 * Sets aspect ratio preservation mode for the top left corner the item.
		 * @param mode Aspect ratio preservation mode for the top left corner of the item.
		 */
		void setPositionAspectMode(AspectMode mode);

		/**
		 * Adds child element to the end of this element's list of children.
		 * Child element should be controlled by the same UI manager as this element.
		 * This element takes ownership of the added element.
		 * All children elements are destroyed when their parent element is destroyed.
		 * @param element Pointer to the child element to add.
		 */
		void addChild(Element * element);

		/**
		 * Removes this element from the parent element.
		 * After calling this method, UI manager becomes responsible for destruction of this element.
		 */
		void removeFromParent();

		/**
		 * Measures contents of the element for the means of layouting.
		 * @param sz Size of the area available to the element.
		 * @param horz Horizontal size constraint.
		 * @param vert Vertical size constraint.
		 * @return Size of the contents of the element.
		 */
		glm::vec2 measureSize(const glm::vec2 & sz,
			SizeConstraint horz = AT_MOST, SizeConstraint vert = AT_MOST) const;

	private:
		std::string m_ID;
		ManagerPtr m_Manager;
		const Layout * m_Layout;
		Element * m_Parent;
		Element * m_FirstChild;
		Element * m_LastChild;
		Element * m_Prev;
		Element * m_Next;
		glm::vec2 m_Position;
		glm::vec2 m_Size;
		glm::vec2 m_DesiredPosition;
		glm::vec2 m_DesiredSize;
		float m_Weight;
		Align m_Align;
		AspectMode m_AspectMode;
		AspectMode m_PositionAspectMode;
		bool m_HasDesiredSize;

		Element(const std::string & elemID, const ManagerPtr & mgr);

		bool removeFromParentChildrenList();

		Element(const Element &) = delete;
		Element & operator=(const Element &) = delete;

		FRIEND_MAKE_PTR()
		friend class Layout;
		friend class Manager;
	};
}

#endif
