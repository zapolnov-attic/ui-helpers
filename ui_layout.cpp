/* vim: set ai noet ts=4 sw=4 tw=115: */
//
// Copyright (c) 2014 Nikolay Zapolnov (zapolnov@gmail.com).
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
#include "ui_layout.h"
#include "ui_element_delegate.h"

UI::Layout::Layout()
{
}

UI::Layout::~Layout()
{
}

float UI::Layout::scaleFactorForElement(const UI::Element * elem, PreferredDimension dimension)
{
	switch (elem->aspectMode())
	{
	case ASPECT_FIT: {
		const glm::vec2 & scale = elem->manager()->scale();
		return std::min(scale.x, scale.y);
		}

	case ASPECT_EXPAND: {
		const glm::vec2 & scale = elem->manager()->scale();
		return std::max(scale.x, scale.y);
		}

	case ASPECT_HORZ: {
		const glm::vec2 & scale = elem->manager()->scale();
		return scale.x;
		}

	case ASPECT_VERT: {
		const glm::vec2 & scale = elem->manager()->scale();
		return scale.y;
		}

	case ASPECT_AVERAGE: {
		const glm::vec2 & scale = elem->manager()->scale();
		return (scale.x + scale.y) * 0.5f;
		}

	default:
	case ASPECT_IGNORE: {
		const glm::vec2 & scale = elem->manager()->scale();
		switch (dimension)
		{
		case UI::Layout::PreferHeight: return scale.y;
		case UI::Layout::PreferWidth: return scale.x;
		case UI::Layout::PreferSmaller: return std::min(scale.x, scale.y);
		case UI::Layout::PreferLarger: return std::max(scale.x, scale.y);
		}
		return scale.x;
		}
	}
}

static glm::vec2 scalePoint(UI::AspectMode mode, const glm::vec2 & pt, const glm::vec2 & scale)
{
	switch (mode)
	{
	case UI::ASPECT_FIT:
		return pt * std::min(scale.x, scale.y);

	case UI::ASPECT_EXPAND:
		return pt * std::max(scale.x, scale.y);

	case UI::ASPECT_HORZ:
		return pt * scale.x;

	case UI::ASPECT_VERT:
		return pt * scale.y;

	case UI::ASPECT_AVERAGE:
		return pt * (scale.x + scale.y) * 0.5f;

	default:
	case UI::ASPECT_IGNORE:
		return pt * scale;
	}
}

glm::vec2 UI::Layout::scaleElement(const UI::Element * elem, const glm::vec2 & sz)
{
	return scalePoint(elem->aspectMode(), sz, elem->manager()->scale());
}

glm::vec2 UI::Layout::scaleElementPosition(const UI::Element * elem, const glm::vec2 & pos)
{
	return scalePoint(elem->positionAspectMode(), pos, elem->manager()->scale());
}

glm::vec2 UI::Layout::positionElementInRect(const UI::Element * elem, const glm::vec2 & elemSize,
	const glm::vec2 & rectPos, const glm::vec2 & rectSize)
{
	glm::vec2 pos = rectPos;

	switch (elem->horzAlign())
	{
	case ALIGN_HCENTER:
		pos.x += (rectSize.x - elemSize.x) * 0.5f;
		break;

	case ALIGN_RIGHT:
		pos.x += rectSize.x - elemSize.x;
		break;

	default:
	case ALIGN_LEFT:
	case ALIGN_UNSPECIFIED:
		break;
	}

	switch (elem->vertAlign())
	{
	case ALIGN_VCENTER:
		pos.y += (rectSize.y - elemSize.y) * 0.5f;
		break;

	case ALIGN_BOTTOM:
		pos.y += rectSize.y - elemSize.y;
		break;

	default:
	case ALIGN_TOP:
	case ALIGN_UNSPECIFIED:
		break;
	}

	return pos;
}

void UI::Layout::setElementLayout(UI::Element * elem, const glm::vec2 & pos, const glm::vec2 & sz)
{
	if (elem->m_Position != pos || elem->m_Size != sz)
	{
		elem->m_Position = pos;
		elem->m_Size = sz;
		if (elem->delegate)
			elem->delegate->onElementLayoutChanged(elem, pos, sz);
	}
}
