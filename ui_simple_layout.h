/* vim: set ai noet ts=4 sw=4 tw=115: */
//
// Copyright (c) 2014 Nikolay Zapolnov (zapolnov@gmail.com).
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
#ifndef __5b8a201d47a058a927e0cb8d23129db8__
#define __5b8a201d47a058a927e0cb8d23129db8__

#include "ui_layout.h"

namespace UI
{
	/**
	 * Simple layouting algorithm.
	 * This algorithm allocates an entire viewport for all elements.
	 */
	class SimpleLayout : public Layout
	{
	public:
		/** Returns pointer to the instance of a simple layouting algorithm. */
		inline static const SimpleLayout * instance() { return &m_Instance; }

		/**
		 * Performs layouting for the list of UI elements.
		 * @param firstElement Pointer to the first UI element.
		 * @param size Size of the viewport.
		 */
		void layoutElements(Element * firstElement, const glm::vec2 & size) const override final;

	private:
		static const SimpleLayout m_Instance;

		SimpleLayout();
		~SimpleLayout();

		SimpleLayout(const SimpleLayout &) = delete;
		SimpleLayout & operator=(const SimpleLayout &) = delete;
	};
}

#endif
