/* vim: set ai noet ts=4 sw=4 tw=115: */
//
// Copyright (c) 2014 Nikolay Zapolnov (zapolnov@gmail.com).
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
#ifndef __93225bb9bf181ba8e83876676b936577__
#define __93225bb9bf181ba8e83876676b936577__

#include <string>

namespace UI
{
	/** Alignment of the UI item. */
	enum Align
	{
		ALIGN_UNSPECIFIED = 0,		/**< Default alignment. */

		ALIGN_LEFT = 0x01,			/**< UI item should be aligned to the left side. */
		ALIGN_RIGHT = 0x02,			/**< UI item should be aligned to the right side. */
		ALIGN_HCENTER = 0x03,		/**< UI item should be horizontally centered. */

		ALIGN_TOP = 0x10,			/**< UI item should be aligned to the top side. */
		ALIGN_BOTTOM = 0x20,		/**< UI item should be aligned to the bottom side. */
		ALIGN_VCENTER = 0x30,		/**< UI item should be vertically centered. */

		/** UI item should be aligned to the top left corner. */
		ALIGN_TOP_LEFT = ALIGN_TOP | ALIGN_LEFT,
		/** UI item should be aligned to the top right corner. */
		ALIGN_TOP_RIGHT = ALIGN_TOP | ALIGN_RIGHT,
		/** UI item should be aligned to the bottom left corner. */
		ALIGN_BOTTOM_LEFT = ALIGN_BOTTOM | ALIGN_LEFT,
		/** UI item should be aligned to the bottom right corner. */
		ALIGN_BOTTOM_RIGHT = ALIGN_BOTTOM | ALIGN_RIGHT,
		/** UI item should be centered. */
		ALIGN_CENTER = ALIGN_HCENTER | ALIGN_VCENTER,

		/** Bit mask for the horizontal alignment. */
		ALIGN_HORIZONTAL_BITMASK = 0x03,
		/** Bit mask for the vertical alignment. */
		ALIGN_VERTICAL_BITMASK = 0x30,
	};

	/**
	 * Converts string representation of an alignment into the corresponding enumeration value.
	 * @param str String representation of an alignment.
	 * @return Enumeration value.
	 * @throw std::runtime_error if given string does not contain a valid alignment.
	 */
	Align alignFromString(const std::string & str);
}

#endif
