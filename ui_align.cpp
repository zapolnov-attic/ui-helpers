/* vim: set ai noet ts=4 sw=4 tw=115: */
//
// Copyright (c) 2014 Nikolay Zapolnov (zapolnov@gmail.com).
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
#include "ui_align.h"
#include <yip-imports/cxx-util/fmt.h>
#include <stdexcept>
#include <unordered_map>

static const std::unordered_map<std::string, UI::Align> g_AlignmentNames = {
	{ "left", UI::ALIGN_LEFT },
	{ "right", UI::ALIGN_RIGHT },
	{ "hcenter", UI::ALIGN_HCENTER },
	{ "top", UI::ALIGN_TOP },
	{ "bottom", UI::ALIGN_BOTTOM },
	{ "vcenter", UI::ALIGN_VCENTER },
	{ "center", UI::ALIGN_CENTER },
};

UI::Align UI::alignFromString(const std::string & str)
{
	UI::Align align = UI::ALIGN_UNSPECIFIED;
	size_t off, pos = 0;

	do
	{
		off = str.find(',', pos);
		std::string name = (off != std::string::npos ? str.substr(pos, off - pos) : str.substr(pos));

		auto it = g_AlignmentNames.find(name);
		if (it == g_AlignmentNames.end())
			throw std::runtime_error(fmt() << "invalid alignment '" << name << "'.");

		align = static_cast<UI::Align>(align | it->second);

		pos = off + 1;
	}
	while (off != std::string::npos);

	return align;
}
