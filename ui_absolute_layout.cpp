/* vim: set ai noet ts=4 sw=4 tw=115: */
//
// Copyright (c) 2014 Nikolay Zapolnov (zapolnov@gmail.com).
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
#include "ui_absolute_layout.h"
#include "ui_element.h"

const UI::AbsoluteLayout UI::AbsoluteLayout::m_Instance;

UI::AbsoluteLayout::AbsoluteLayout()
{
}

UI::AbsoluteLayout::~AbsoluteLayout()
{
}

void UI::AbsoluteLayout::layoutElements(Element * firstElement, const glm::vec2 & size) const
{
	for (Element * p = firstElement; p; p = p->nextSibling())
	{
		glm::vec2 scale = p->manager()->scale();

		glm::vec2 areaSize, elemSize;
		if (!p->hasDesiredSize())
		{
			elemSize = p->measureSize(size, UI::UNSPECIFIED, UI::UNSPECIFIED);
			areaSize = elemSize;
		}
		else
		{
			glm::vec2 desiredSize = p->desiredSize();
			areaSize = desiredSize * scale;
			elemSize = scaleElement(p, desiredSize);
		}

		glm::vec2 areaPos = scaleElementPosition(p, p->desiredPosition());
		glm::vec2 elemPos = positionElementInRect(p, elemSize, areaPos, areaSize);

		setElementLayout(p, elemPos, elemSize);
		p->layout()->layoutElements(p->firstChild(), elemSize);
	}
}
