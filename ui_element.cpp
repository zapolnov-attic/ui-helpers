/* vim: set ai noet ts=4 sw=4 tw=115: */
//
// Copyright (c) 2014 Nikolay Zapolnov (zapolnov@gmail.com).
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
#include "ui_element.h"
#include "ui_element_delegate.h"
#include "ui_simple_layout.h"
#include <cassert>

UI::Element::Element(const std::string & elemID, const ManagerPtr & mgr)
	: m_ID(elemID),
	  m_Manager(mgr),
	  m_Layout(nullptr),
	  m_Parent(nullptr),
	  m_FirstChild(nullptr),
	  m_LastChild(nullptr),
	  m_Prev(nullptr),
	  m_Next(nullptr),
	  m_Weight(1.0f),
	  m_Align(ALIGN_UNSPECIFIED),
	  m_AspectMode(ASPECT_IGNORE),
	  m_PositionAspectMode(ASPECT_IGNORE),
	  m_HasDesiredSize(false)
{
}

UI::Element::~Element()
{
	assert(!m_Manager->layouting());

	while (m_LastChild)
		delete m_LastChild;

	if (m_Parent)
		removeFromParentChildrenList();
	else
		m_Manager->removeRootElement(this);

	m_Manager->setNeedsLayout();
	m_Manager->m_Elements.erase(m_ID);

	if (delegate)
		delegate->onElementDestroyed(this);
}

const UI::Layout * UI::Element::layout() const
{
	if (m_Layout)
		return m_Layout;

	if (m_Parent)
		return m_Parent->layout();

	return SimpleLayout::instance();
}

void UI::Element::setLayout(const Layout * layout)
{
	assert(!m_Manager->layouting());

	if (m_Layout != layout)
	{
		m_Layout = layout;
		m_Manager->setNeedsLayout();
	}
}

void UI::Element::setDesiredPosition(const glm::vec2 & pos)
{
	assert(!m_Manager->layouting());

	if (m_DesiredPosition != pos)
	{
		m_DesiredPosition = pos;
		m_Manager->setNeedsLayout();
	}
}

void UI::Element::setDesiredSize(const glm::vec2 & sz)
{
	assert(!m_Manager->layouting());

	if (m_DesiredSize != sz || !m_HasDesiredSize)
	{
		m_HasDesiredSize = true;
		m_DesiredSize = sz;
		m_Manager->setNeedsLayout();
	}
}

void UI::Element::resetDesiredSize()
{
	assert(!m_Manager->layouting());

	if (m_HasDesiredSize)
	{
		m_HasDesiredSize = false;
		m_DesiredSize = glm::vec2();
		m_Manager->setNeedsLayout();
	}
}

void UI::Element::setWeight(float w)
{
	assert(!m_Manager->layouting());

	if (w < 0.0f)
		w = 0.0f;

	if (m_Weight != w)
	{
		m_Weight = w;
		m_Manager->setNeedsLayout();
	}
}

void UI::Element::setAlign(Align alignment)
{
	assert(!m_Manager->layouting());

	if (m_Align != alignment)
	{
		m_Align = alignment;
		m_Manager->setNeedsLayout();
	}
}

void UI::Element::setAspectMode(AspectMode mode)
{
	assert(!m_Manager->layouting());

	if (m_AspectMode != mode)
	{
		m_AspectMode = mode;
		m_Manager->setNeedsLayout();
	}
}

void UI::Element::setPositionAspectMode(AspectMode mode)
{
	assert(!m_Manager->layouting());

	if (m_PositionAspectMode != mode)
	{
		m_PositionAspectMode = mode;
		m_Manager->setNeedsLayout();
	}
}

void UI::Element::addChild(Element * element)
{
	assert(!m_Manager->layouting());
	assert(element != nullptr);
	assert(element != this);
	assert(element->manager() == m_Manager);

	if (element->m_Parent)
		element->removeFromParentChildrenList();
	else
		m_Manager->removeRootElement(element);

	// Check for circular reference
  #ifdef NDEBUG
	for (Element * parent = m_Parent; parent; parent = parent->m_Parent)
		assert(parent != element);
  #endif

	Element * oldParent = element->m_Parent;
	element->m_Parent = this;

	element->m_Next = nullptr;
	element->m_Prev = m_LastChild;

	if (!m_LastChild)
		m_FirstChild = element;
	else
		m_LastChild->m_Next = element;
	m_LastChild = element;

	m_Manager->setNeedsLayout();

	if (oldParent != this && delegate)
		element->delegate->onElementParentChanged(element, oldParent, this);
}

void UI::Element::removeFromParent()
{
	if (removeFromParentChildrenList())
	{
		m_Manager->addRootElement(this);
		if (delegate)
			delegate->onElementParentChanged(this, m_Parent, nullptr);
	}
}

glm::vec2 UI::Element::measureSize(const glm::vec2 & sz, SizeConstraint horz, SizeConstraint vert) const
{
	if (delegate)
		return delegate->measureElementSize(this, sz, horz, vert);

	if (m_HasDesiredSize)
		return UI::Layout::scaleElement(this, m_DesiredSize);

	return sz;
}

bool UI::Element::removeFromParentChildrenList()
{
	assert(!m_Manager->layouting());

	if (!m_Parent)
		return false;

	if (m_Next)
		m_Next->m_Prev = m_Prev;
	else
		m_Parent->m_LastChild = m_Prev;

	if (m_Prev)
		m_Prev->m_Next = m_Next;
	else
		m_Parent->m_FirstChild = m_Next;

	m_Next = nullptr;
	m_Prev = nullptr;
	m_Parent = nullptr;

	return true;
}
