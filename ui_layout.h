/* vim: set ai noet ts=4 sw=4 tw=115: */
//
// Copyright (c) 2014 Nikolay Zapolnov (zapolnov@gmail.com).
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
#ifndef __007e7205e254b7a5d84f69834016c40f__
#define __007e7205e254b7a5d84f69834016c40f__

#include "ui_element.h"
#include <vector>

namespace UI
{
	/** Base class for various layouting algorithms. */
	class Layout
	{
	public:
		/** Preferred dimension. */
		enum PreferredDimension
		{
			PreferWidth = 0,			/**< Prefer width over height. */
			PreferHeight,				/**< Prefer height over width. */
			PreferSmaller,				/**< Prefer dimension that is smaller. */
			PreferLarger,				/**< Prefer dimension that is larger. */
		};

		/**
		 * Performs layouting for the list of UI elements.
		 * @param firstElement Pointer to the first UI element.
		 * @param size Size of the viewport.
		 */
		virtual void layoutElements(Element * firstElement, const glm::vec2 & size) const = 0;

		/**
		 * Retrieves scale factor for the given element according to the element's aspect ratio preservation mode.
		 * @param elem Pointer to the element.
		 * @param dimension Dimension to prefer for the "ignore" aspect ratio mode.
		 * @return Scale factor.
		 */
		static float scaleFactorForElement(const UI::Element * elem, PreferredDimension dimension);

		/**
		 * Scales given size according to the element's aspect ratio preservation mode.
		 * @param elem Pointer to the element.
		 * @param sz Size to scale.
		 * @return Scaled size.
		 */
		static glm::vec2 scaleElement(const UI::Element * elem, const glm::vec2 & sz);

		/**
		 * Scales given position according to the element's top left corner aspect ratio preservation mode.
		 * @param elem Pointer to the element.
		 * @param pos Position to scale.
		 * @return Scaled position.
		 */
		static glm::vec2 scaleElementPosition(const UI::Element * elem, const glm::vec2 & pos);

		/**
		 * Calculates position of the UI element in the specified rectangle according to element's
		 * alignment rules.
		 * @param elem Pointer to the element.
		 * @param elemSize Size of the element.
		 * @param rectPos Coordinates of the top left corner of the rectangle.
		 * @param rectSize Size of the rectangle.
		 * @return Coordinates of the top left corner of the element.
		 */
		static glm::vec2 positionElementInRect(const UI::Element * elem, const glm::vec2 & elemSize,
			const glm::vec2 & rectPos, const glm::vec2 & rectSize);

	protected:
		/** Constructor. */
		Layout();

		/** Destructor. */
		~Layout();

		/**
		 * Sets position and size of the specified UI element.
		 * @param elem Pointer to the element.
		 * @param pos New coordinates of the top left corner of the element relative to its parent element.
		 * @param sz New size of this element.
		 */
		static void setElementLayout(UI::Element * elem, const glm::vec2 & pos, const glm::vec2 & sz);

		Layout(const Layout &) = delete;
		Layout & operator=(const Layout &) = delete;
	};
}

#endif
