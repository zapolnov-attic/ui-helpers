/* vim: set ai noet ts=4 sw=4 tw=115: */
//
// Copyright (c) 2014 Nikolay Zapolnov (zapolnov@gmail.com).
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
#include "ui_manager.h"
#include "ui_absolute_layout.h"
#include "ui_simple_layout.h"
#include "ui_horizontal_layout.h"
#include "ui_vertical_layout.h"
#include "ui_element.h"
#include "ui_element_delegate.h"
#include <cassert>
#include <exception>
#include <yip-imports/cxx-util/fmt.h>
#include <yip-imports/cxx-util/macros.h>
#include <yip-imports/tinyxml-util.h>

UI::Manager::Manager(const glm::vec2 & origSize)
	: m_OriginalSize(origSize),
	  m_ViewportSize(origSize),
	  m_Scale(1.0f, 1.0f),
	  m_FirstRootElement(nullptr),
	  m_LastRootElement(nullptr),
	  m_Layouting(false),
	  m_NeedsLayout(false)
{
}

UI::Manager::~Manager()
{
	while (m_FirstRootElement)
		delete m_FirstRootElement;
}

UI::ManagerPtr UI::Manager::create(const glm::vec2 & origSize)
{
	return make_ptr<UI::Manager>(origSize);
}

UI::ManagerPtr UI::Manager::createFromXML(const TiXmlElement * xmlElem, const DelegateFactory & dfact)
{
	TiXmlAttribute * sizeAttr = xmlElem->GetAttribute("size");
	if (UNLIKELY(!sizeAttr))
		throw std::runtime_error(xmlMissingAttribute(xmlElem, "size"));

	std::vector<float> size;
	if (UNLIKELY(!xmlAttrToCommaSeparatedFloatList(sizeAttr, size) || size.size() != 2))
		throw std::runtime_error(xmlInvalidAttributeValue(sizeAttr));

	UI::ManagerPtr manager = make_ptr<UI::Manager>(glm::vec2(size[0], size[1]));

	for (const TiXmlElement * xmlChild = xmlElem->FirstChildElement();
			xmlChild; xmlChild = xmlChild->NextSiblingElement())
		manager->createElementFromXML(xmlChild, dfact);

	return manager;
}

void UI::Manager::setViewportSize(const glm::vec2 & viewSize)
{
	if (m_ViewportSize != viewSize)
	{
		m_ViewportSize = viewSize;
		m_Scale = m_ViewportSize / m_OriginalSize;
		setNeedsLayout();
	}
}

UI::Element * UI::Manager::createElement(const std::string & id, ElementDelegate * delegate)
{
	Element * element;

	if (UNLIKELY(m_Elements.find(id) != m_Elements.end()))
		throw std::runtime_error(fmt() << "duplicate element id '" << id << "'.");

	try {
		element = new Element(id, shared_from_this());
	} catch (...) {
		delegate->onElementDestroyed(nullptr);
		throw;
	}

	element->delegate = delegate;

	try {
		addRootElement(element);
		m_Elements.insert(std::make_pair(element->id(), element));
	} catch (...) {
		delete element;
		throw;
	}

	return element;
}

UI::Element * UI::Manager::createElementFromXML(const TiXmlElement * xmlElem, const DelegateFactory & dfact,
	Element * parent)
{
	const char * id = xmlElem->Attribute("id");
	Element * element = new Element(id ? id : "", shared_from_this());

	try
	{
		addRootElement(element);
		m_Elements.insert(std::make_pair(element->id(), element));

		element->delegate = dfact.delegateForClass(xmlElem->ValueStr());

		for (const TiXmlAttribute * attr = xmlElem->FirstAttribute(); attr; attr = attr->Next())
		{
			std::string attrName = attr->Name();
			if (attrName == "id")
				continue;
			else if (attrName == "pos")
			{
				std::vector<float> pos;
				if (UNLIKELY(!xmlAttrToCommaSeparatedFloatList(attr, pos) || pos.size() != 2))
					throw std::runtime_error(xmlInvalidAttributeValue(attr));
				element->setDesiredPosition(glm::vec2(pos[0], pos[1]));
			}
			else if (attrName == "size")
			{
				std::vector<float> size;
				if (UNLIKELY(!xmlAttrToCommaSeparatedFloatList(attr, size) || size.size() != 2))
					throw std::runtime_error(xmlInvalidAttributeValue(attr));
				element->setDesiredSize(glm::vec2(size[0], size[1]));
			}
			else if (attrName == "weight")
			{
				float weight = 1.0f;
				if (UNLIKELY(!xmlAttrToFloat(attr, weight)))
					throw std::runtime_error(xmlInvalidAttributeValue(attr));
				element->setWeight(weight);
			}
			else if (attrName == "align")
			{
				try {
					element->setAlign(alignFromString(attr->ValueStr()));
				} catch (const std::exception & e) {
					throw std::runtime_error(xmlError(attr, e.what()));
				}
			}
			else if (attrName == "aspect")
			{
				try {
					element->setAspectMode(aspectModeFromString(attr->ValueStr()));
				} catch (const std::exception & e) {
					throw std::runtime_error(xmlError(attr, e.what()));
				}
			}
			else if (attrName == "posAspect")
			{
				try {
					element->setPositionAspectMode(aspectModeFromString(attr->ValueStr()));
				} catch (const std::exception & e) {
					throw std::runtime_error(xmlError(attr, e.what()));
				}
			}
			else if (attrName == "layout")
			{
				if (attr->ValueStr() == "simple")
					element->setLayout(SimpleLayout::instance());
				else if (attr->ValueStr() == "absolute")
					element->setLayout(AbsoluteLayout::instance());
				else if (attr->ValueStr() == "horizontal")
					element->setLayout(HorizontalLayout::instance());
				else if (attr->ValueStr() == "vertical")
					element->setLayout(VerticalLayout::instance());
				else
				{
					throw std::runtime_error(xmlError(attr,
						fmt() << "invalid layout '" << attr->ValueStr() << "'."));
				}
			}
			else if (element->delegate)
			{
				bool success = false;

				try {
					success = element->delegate->setElementProperty(element, attrName, attr->ValueStr());
				} catch (const std::exception & e) {
					throw std::runtime_error(xmlError(attr, e.what()));
				}

				if (UNLIKELY(!success))
				{
					throw std::runtime_error(xmlError(attr,
						fmt() << "UI element has no attribute '" << attrName << "'."));
				}
			}
			else
			{
				throw std::runtime_error(xmlError(attr,
					fmt() << "UI element has no attribute '" << attrName << "'."));
			}
		}

		for (const TiXmlElement * xmlChild = xmlElem->FirstChildElement();
				xmlChild; xmlChild = xmlChild->NextSiblingElement())
			createElementFromXML(xmlChild, dfact, element);

		if (parent)
			parent->addChild(element);
	}
	catch (...)
	{
		delete element;
		throw;
	}

	return element;
}

void UI::Manager::setNeedsLayout()
{
	if (m_NeedsLayout)
		return;
	m_NeedsLayout = true;
	if (onNeedsLayout)
		onNeedsLayout();
}

void UI::Manager::layoutItems()
{
	assert(!m_Layouting);
	m_Layouting = true;

	try
	{
		SimpleLayout::instance()->layoutElements(m_FirstRootElement, m_ViewportSize);
	}
	catch (...)
	{
		m_Layouting = false;
		throw;
	}

	m_Layouting = false;
	m_NeedsLayout = false;
}

UI::Element * UI::Manager::getElementById(const std::string & id) const
{
	auto it = m_Elements.find(id);
	return (it != m_Elements.end() ? it->second : nullptr);
}

void UI::Manager::addRootElement(Element * element)
{
	assert(!m_Layouting);
	assert(!element->m_Parent);

	element->m_Next = nullptr;
	element->m_Prev = m_LastRootElement;

	if (!m_LastRootElement)
		m_FirstRootElement = element;
	else
		m_LastRootElement->m_Next = element;

	m_LastRootElement = element;
	setNeedsLayout();
}

void UI::Manager::removeRootElement(Element * element)
{
	assert(!m_Layouting);
	assert(!element->m_Parent);

	if (element->m_Next)
		element->m_Next->m_Prev = element->m_Prev;
	else
		m_LastRootElement = element->m_Prev;

	if (element->m_Prev)
		element->m_Prev->m_Next = element->m_Next;
	else
		m_FirstRootElement = element->m_Next;
}
