/* vim: set ai noet ts=4 sw=4 tw=115: */
//
// Copyright (c) 2014 Nikolay Zapolnov (zapolnov@gmail.com).
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
#include "ui_vertical_layout.h"
#include "ui_element.h"

const UI::VerticalLayout UI::VerticalLayout::m_Instance;

UI::VerticalLayout::VerticalLayout()
{
}

UI::VerticalLayout::~VerticalLayout()
{
}

void UI::VerticalLayout::layoutElements(Element * firstElement, const glm::vec2 & size) const
{
	// Calculate total weight for all elements

	float totalWeight = 0.0f;
	size_t numElements = 0;
	for (Element * p = firstElement; p; p = p->nextSibling())
	{
		totalWeight += p->weight();
		++numElements;
	}

	// Determine heights for elements

	std::vector<float> heights;
	heights.reserve(numElements);
	for (Element * p = firstElement; p; p = p->nextSibling())
	{
		float height = (totalWeight > 0.0f ? size.y * (p->weight() / totalWeight) : 0.0f);
		heights.push_back(height);
	}

	// Apply heights to elements

	glm::vec2 areaPos(0.0f, 0.0f);
	auto it = heights.begin();
	for (Element * p = firstElement; p; p = p->nextSibling())
	{
		glm::vec2 areaSize = glm::vec2(size.x, *it++);

		glm::vec2 elemSize;
		if (!p->hasDesiredSize())
			elemSize = p->measureSize(areaSize, UI::AT_MOST, UI::AT_MOST);
		else
			elemSize = scaleElement(p, p->desiredSize());
		elemSize.x = std::min(elemSize.x, areaSize.x);
		elemSize.y = std::min(elemSize.y, areaSize.y);

		glm::vec2 elemPos = positionElementInRect(p, elemSize, areaPos, areaSize);
		areaPos.y += areaSize.y;

		setElementLayout(p, elemPos, elemSize);
		p->layout()->layoutElements(p->firstChild(), elemSize);
	}
}
