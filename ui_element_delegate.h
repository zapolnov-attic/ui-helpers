/* vim: set ai noet ts=4 sw=4 tw=115: */
//
// Copyright (c) 2014 Nikolay Zapolnov (zapolnov@gmail.com).
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
#ifndef __c69ed509afb3d62f318ceedab4b4b03f__
#define __c69ed509afb3d62f318ceedab4b4b03f__

#include "ui_element.h"
#include <string>

namespace UI
{
	/** Base class for UI element delegates. */
	class ElementDelegate
	{
	public:
		/** Destructor. */
		virtual inline ~ElementDelegate() {}

		/**
		 * Measures contents of the element for the means of layouting.
		 * @param element Pointer to the element.
		 * @param sz Size of the area available to the element.
		 * @param horz Horizontal size constraint.
		 * @param vert Vertical size constraint.
		 * @return Size of the contents of the element.
		 */
		virtual glm::vec2 measureElementSize(const Element * element, const glm::vec2 & sz,
			SizeConstraint horz = AT_MOST, SizeConstraint vert = AT_MOST);

		/**
		 * Sets property of the element.
		 * @param element Pointer to the element.
		 * @param name Name of the property.
		 * @param value Value of the property.
		 * @return *true* on success or *false* if element does not have such property.
		 */
		virtual bool setElementProperty(Element * element, const std::string & name, const std::string & value);

		/**
		 * Called when element is being destroyed.
		 * @param element Pointer to the element. This could be *nullptr* if an exception has occured
		 * while constructing the element.
		 */
		virtual void onElementDestroyed(Element * element);

		/**
		 * Called when parent of the element changes.
		 * @param element Pointer to the element.
		 * @param oldParent Pointer to the old parent element.
		 * @param newParent Pointer to the new parent element.
		 */
		virtual void onElementParentChanged(Element * element, Element * oldParent, Element * newParent);

		/**
		 * Called when position or size of this element changes.
		 * @param element Pointer to the element.
		 * @param pos New coordinates of the top left corner of this element relative to the parent element.
		 * @param sz New size of this element.
		 */
		virtual void onElementLayoutChanged(Element * element, const glm::vec2 & pos, const glm::vec2 & sz);
	};
}

#endif
