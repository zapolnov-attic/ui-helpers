/* vim: set ai noet ts=4 sw=4 tw=115: */
//
// Copyright (c) 2014 Nikolay Zapolnov (zapolnov@gmail.com).
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
#ifndef __a46c9ba1a7e3d4d15b6e052d1dc56e0f__
#define __a46c9ba1a7e3d4d15b6e052d1dc56e0f__

#include "ui_layout.h"

namespace UI
{
	/** Positions UI elements vertically one-by-one. */
	class VerticalLayout : public Layout
	{
	public:
		/** Returns pointer to the instance of a vertical layouting algorithm. */
		inline static const VerticalLayout * instance() { return &m_Instance; }

		/**
		 * Performs layouting for the list of UI elements.
		 * @param firstElement Pointer to the first UI element.
		 * @param size Size of the viewport.
		 */
		void layoutElements(Element * firstElement, const glm::vec2 & size) const override final;

	private:
		static const VerticalLayout m_Instance;

		VerticalLayout();
		~VerticalLayout();

		VerticalLayout(const VerticalLayout &) = delete;
		VerticalLayout & operator=(const VerticalLayout &) = delete;
	};
}

#endif
